import requests
import os
import sys
import json
from datetime import datetime
from os.path import exists

played_video_IDs = []
path_to_mpv = ""
video_duration_limit = 0
mpv_base_volume = 0

def extractAudioURLfromVideoID(video_id):
    video_url = "https://piped-api.garudalinux.org/streams/"+video_id
    json = requests.get(video_url).json()
    audio_Streams_JSON = json['audioStreams']
    num_of_audio_streams = len(json['audioStreams'])
    last_audio_stream_URL = audio_Streams_JSON[num_of_audio_streams-1]["url"]
    print(json['title'])
    print("----- ")
    return last_audio_stream_URL

def inputIntoVideoID(data_input):
    input_split = data_input.split("v=")
    return input_split[len(input_split)-1]

def playAudioURLwithMPV(audio_url):
    mpv_param_str = " --volume={mpv_volume} --no-resume-playback \"{url}\"".format(mpv_volume=mpv_base_volume, url=audio_url)
    if sys.platform == "linux":
        os.system("mpv " + mpv_param_str) 
    elif sys.platform == "win32":
        os.system(path_to_mpv+"\\mpv.com "+mpv_param_str)

def extractRelatedVideoIDfromVideoID(video_id):
    video_url = "https://piped-api.garudalinux.org/streams/"+video_id
    json = requests.get(video_url).json()
    related_Streams = json['relatedStreams']
    num_of_related_streams = len(json['relatedStreams'])
    related_video_id = ""
    index = 0
    while True:
        related_video_id = inputIntoVideoID(related_Streams[index]['url'])
        invidious_JSON = getInvidiousJSONforVideoID(related_video_id)
        if checkFiltersForRelatedVideoID(related_video_id, related_Streams[index], invidious_JSON):
            break
        index += 1

    dt = datetime.now()
    print("----- ")
    print("TimeStamp: ", dt)
    print("https://piped.garudalinux.org/watch?v=" + related_video_id)
    return related_video_id

def playAudio(data_input): 
    video_id = inputIntoVideoID(data_input)
    audio_url = extractAudioURLfromVideoID(video_id)
    playAudioURLwithMPV(audio_url)
    played_video_IDs.append(video_id)
    video_id = extractRelatedVideoIDfromVideoID(video_id)
    playAudio(video_id)

def checkFiltersForRelatedVideoID(video_id, stream_json, invidious_JSON):
    try:
        if video_id in played_video_IDs:
            return False 
        elif stream_json['duration'] > video_duration_limit:
            return False
        elif "Music" not in invidious_JSON['genre']:
           return False
    except Exception as e:
        print("The error raised is: ", e)
        print(invidious_JSON)
        return False

    return True

def getInvidiousJSONforVideoID(video_id):
    video_url = "https://invidious.sethforprivacy.com/api/v1/videos/"+video_id
    invidious_json = requests.get(video_url).json()
    return invidious_json

if __name__ == '__main__':
    args_len = len(sys.argv)
    if args_len > 1 and (len(sys.argv[1]) == 11 or "v=" in sys.argv[1]):
        script_dir = os.path.abspath(os.path.dirname(__file__))
        if exists(script_dir+'/conf.json'):
            conf_file = open(script_dir+'/conf.json')
            conf_json = json.load(conf_file)
            conf_file.close()
            mpv_base_volume = conf_json['mpv_base_volume']
            video_duration_limit = conf_json['video_duration_limit_s']
            if sys.platform == "win32":
                path_to_mpv = conf_json['path_to_mpv']
                if len(path_to_mpv) < 1:
                    path_to_mpv = input("Enter the location of the MPV installation folder: ")
                    conf_json['path_to_mpv'] = path_to_mpv
                    conf_file = open(script_dir+'/conf.json','w')
                    json.dump(conf_json, conf_file, indent=2)
                    conf_file.close()

        user_input = sys.argv[1]
        playAudio(user_input)